/*
 * EIA tone generator project 
 * by BDJ/RAB DPS/Devlab
 *  
 * 12/6/18
 * v.001
 * 
 * Modified as a single tone solution
 * 
 * 
 * 
 * 
 */

#include <mcp4261.h>
#include <SPI.h>
#include <FastLED.h>

//const int SPI_CLOCK = 13; //SPI pins ////FastLED.h library defines this pin already////
const int SPI_MOSI = 11;
const int SPI_MISO = 12;

 //Chip select pin for mcp4261 
const int POT_0_CS = 10;
const int POT_1_CS = 9;

MCP4261 pot0 = MCP4261(POT_0_CS);   //ditital pot chip select 
MCP4261 pot1 = MCP4261(POT_1_CS);

int wiper_0_pot_0 = 200; //starts digipot wiper position at 200
int wiper_1_pot_0 = 200;
int wiper_0_pot_1 = 200;
int wiper_1_pot_1 = 200;

const int freq1_pin = 5; //2175 pinout
const int freq2_pin = 3; //1004 pinout






unsigned long currentMillis = 0;       //tracks milliseconds board is on

 


const int fr1 = 2175;
const int fr2 = 2050;
const int fr3 = 1004;

char key_in;
/**********************************
 ********* LED SECTION ************
 **********************************
 */
 #include "FastLED.h"

// How many leds are in the strip?
#define NUM_LEDS 2

// Data pin that led data will be written out over
#define DATA_PIN 6

// Clock pin only needed for SPI based chipsets when not using hardware SPI
//#define CLOCK_PIN 8

// This is an array of leds.  One item for each led in your strip.
CRGB leds[NUM_LEDS];

//Button Pins
const int Button1 = 7;
const int Button2 = 8;

//Button States (to control things)
byte Button1_state = LOW;
byte Button2_state = LOW;

//Button timer / Button Interval
unsigned long Button1_Millis = 0;  //button 1 timer
unsigned long Button2_Millis = 0;  //button 2 timer
const int Button_interval = 300;  //used for both buttons
/*******************************************************************************
 *************************SETUP*************************************************
 *******************************************************************************
 */
void setup() {
  // Put code Here
    // Setup SPI communications
    SPI.setDataMode(SPI_MODE0);
    SPI.setBitOrder(MSBFIRST);
    SPI.setClockDivider(SPI_CLOCK_DIV8);
   SPI.begin();

      pot0.initialize(); //initializes pot0 & 1
      pot1.initialize();
 Serial.begin(9600);
 Serial.println("EIA Tone Generator booted");
    //sets pins
 pinMode(freq1_pin, OUTPUT);
 pinMode(freq2_pin, OUTPUT);
 pinMode(POT_0_CS , OUTPUT);
 pinMode(POT_1_CS , OUTPUT);
 pinMode(Button1 , INPUT_PULLUP);
 pinMode(Button2 , INPUT_PULLUP);
 
 FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);

      leds[0] = CRGB::Green;   //toggles LED's at startup
      FastLED.show();
        delay(200);
      leds[1] = CRGB::Blue;
      FastLED.show();
             delay(800);
      leds[0] = CRGB::Black;
      FastLED.show();
        delay(200);
      leds[1] = CRGB::Black;
      FastLED.show();
             delay(10); 
}      
/******************************************************************************
 ******************************LOOP********************************************
 ******************************************************************************
 */
void loop() {
  // track timing
//currentMicros = micros();
currentMillis = millis();

readButton1();  //pin7
readButton2();  //pin8

//Terminal input
readinput(); //reads serial input from terminal and increments / deincrements digital pots
if (Button1_state == HIGH){
     Serial.println(Button1_state);
     wiper_0_pot_0 = 190;
     wiper_1_pot_1 = 10;
        digipot0_write();
        digipot1_write();
          leds[1] = CRGB::Blue;
          FastLED.show();
     tone(freq1_pin, fr1, 125);
        delay(125);
          wiper_0_pot_0 = 200;
          wiper_1_pot_1 = 177;
            digipot0_write();
            digipot1_write();
              leds[1] = CRGB::Green;
              FastLED.show();  
      tone(freq1_pin, fr2, 40);
          delay(40);
            wiper_0_pot_0 = 200;
            wiper_1_pot_1 = 246;
              digipot0_write();
              digipot1_write();
                leds[1] = CRGB::Red;
                FastLED.show();
       tone(freq1_pin, fr1, 100);
               delay(200);
               Button1_state = LOW;
               noTone(freq1_pin);
                leds[1] = CRGB::Black;
                FastLED.show();
}
if (Button2_state == HIGH){
  //Serial.println(Button2_state);
  noTone(freq1_pin);
      if (wiper_0_pot_0 == 170){
  //  Serial.println(wiper_0_pot_0);
      } else {wiper_0_pot_0 = 170;
        digipot0_write();}
      if (wiper_0_pot_1 == 200){
       //    Serial.println(wiper_0_pot_1); 
        }
        else {
        wiper_0_pot_1 = 200;
        digipot1_write();
      }    
     tone(freq2_pin, fr1); 
        leds[0] = CRGB::Green;
        FastLED.show(); 
}else 
  noTone(freq2_pin);
      leds[0] = CRGB::Black;
      FastLED.show();
//  tone(freq1_pin, fr1);
}
/******************************************************************************
 *************************END OF LOOP****************************************** 
 ****************************************************************************** 
 */

void readButton1() {

     // this only reads the button state after the button interval has elapsed
     // this avoids multiple flashes if the button bounces
     // every time the button is pressed it changes button1_State causing the button to toggle on or off
     // Notice that there is no need to synchronize this use of millis()
 
 if (millis() - Button1_Millis >= Button_interval) {

   if (digitalRead(Button1) == LOW) {
     Button1_state = ! Button1_state; // this changes it to LOW if it was HIGH 
                                          //   and to HIGH if it was LOW
     Button1_Millis += Button_interval;
   }
 }

}
void readButton2() {

     // this only reads the button state after the button interval has elapsed
     // this avoids multiple flashes if the button bounces
     // every time the button is pressed it changes button2_State causing the button to toggle on or off
     // Notice that there is no need to synchronize this use of millis()
 
 //if (millis() - Button2_Millis >= Button_interval) {

   if (digitalRead(Button2) == LOW) {
//     Button2_state = ! Button2_state; // this changes it to LOW if it was HIGH 
       Button2_state = HIGH;                                   //   and to HIGH if it was LOW
 //    Button2_Millis += Button_interval;
  // }
 } else Button2_state = LOW;

}

/*********************************************************************************
 **************Reads Serial input from terminal*********************************** 
 ********************************************************************************* 
 */
                   //reads serial input from terminal
                   //then adjust digital pot up or down 
void readinput() {                  
  if (Serial.available() > 0) {
    key_in = Serial.read();
    key_in = toupper(key_in);
      Serial.print(key_in);
    switch (key_in) {
      case 'A':
        wiper_0_pot_0_inc();
        break;
      case 'Z':
        wiper_0_pot_0_dec();
        break;
      case 'S':
        wiper_1_pot_0_inc();
        break;
      case 'X':
        wiper_1_pot_0_dec();
        break;
      case 'D':
        wiper_0_pot_1_inc();
        break;        
      case 'C':
        wiper_0_pot_1_dec();
        break;
      case 'F':
        wiper_1_pot_1_inc();
        break;
      case 'V':
        wiper_1_pot_1_dec();
        break;        
        
    }
  }

}

/********************************************************************************
 **********Increments/Deincrements Pot Wipers- Commands from serial in*********** 
 ******************************************************************************** 
 */
void wiper_0_pot_0_inc(){            // increments pot of final output amp

      wiper_0_pot_0++;
      Serial.println(wiper_0_pot_0);
  if (wiper_0_pot_0 >= 256){
    wiper_0_pot_0 = 256;
  }
    if (wiper_0_pot_0 <= 0){
    wiper_0_pot_0 = 0;
  }
  digipot0_write();
}

void wiper_0_pot_0_dec(){           // deincrements pot of final output amp

      wiper_0_pot_0--;
      Serial.println(wiper_0_pot_0);  
    if (wiper_0_pot_0 >= 256){
    wiper_0_pot_0 = 256;
  }
    if (wiper_0_pot_0 <= 0){
    wiper_0_pot_0 = 0;
  }
  digipot0_write();
}

void wiper_1_pot_0_inc(){           // increments input level adjustment pot
       
        wiper_1_pot_0++;
        Serial.println(wiper_1_pot_0);  
  if (wiper_1_pot_0 >= 256){
    wiper_1_pot_0 = 256;
  }
    if (wiper_1_pot_0 <= 0){
    wiper_1_pot_0 = 0;
  }
  digipot0_write();
}

void wiper_1_pot_0_dec() {            // deincrements input level adjustment pot

      wiper_1_pot_0--;
      Serial.println(wiper_1_pot_0);  
    if (wiper_1_pot_0 >= 256){
    wiper_1_pot_0 = 256;
  }
    if (wiper_1_pot_0 <= 0){
    wiper_1_pot_0 = 0;
  } 
  digipot0_write(); 
}

void wiper_0_pot_1_inc(){             // increments pot for freq1_pin level adjustment (2175 hz/2050 hz)
        
        wiper_0_pot_1++;
        Serial.println(wiper_0_pot_1);  
  if (wiper_0_pot_1 >= 256){
    wiper_0_pot_1 = 256;
  }
    if (wiper_0_pot_1 <= 0){
    wiper_0_pot_1 = 0;
  }
  digipot1_write();
}

void wiper_0_pot_1_dec(){             // deincrements pot for freq1_pin level adjustment (2175 hz/2050 hz)

      wiper_0_pot_1--;
      Serial.println(wiper_0_pot_1);  
    if (wiper_0_pot_1 >= 256){
    wiper_0_pot_1 = 256;
  }
    if (wiper_0_pot_1 <= 0){
    wiper_0_pot_1 = 0;
  }
  digipot1_write();  
}

void wiper_1_pot_1_inc(){             // increments pot for freq2_pin level adjustment (1004 hz)
      
      wiper_1_pot_1++;
      Serial.println(wiper_1_pot_1);  
    if (wiper_1_pot_1 >= 256){
    wiper_1_pot_1 = 256;
  }
    if (wiper_1_pot_1 <= 0){
    wiper_1_pot_1 = 0;
  }
  digipot1_write();  
}
 
void wiper_1_pot_1_dec() {         // deincrements pot for freq2_pin level adjustment (1004 hz)

      wiper_1_pot_1--;
      Serial.println(wiper_1_pot_1);  
    if (wiper_1_pot_1 >= 256){
    wiper_1_pot_1 = 256;
  }
    if (wiper_1_pot_1 <= 0){
    wiper_1_pot_1 = 0;
  }
  digipot1_write();  
}
/**********************************************************************************
 *****************************write to digital pot*********************************
 **********************************************************************************
 */

void digipot0_write() {            //writes new value to pot0

     pot0.setWiper0(wiper_0_pot_0);
     pot0.setWiper1(wiper_1_pot_0);  
  }
void digipot1_write() {           //writes new value to pot1

     pot1.setWiper0(wiper_0_pot_1);
     pot1.setWiper1(wiper_1_pot_1);    
  }
  
