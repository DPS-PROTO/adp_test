/*
 * EIA tone generator project 
 * by BDJ/RAB DPS/Devlab
 *  
 * 12/6/18
 * v.001
 * 
 * 
 * 
 * 
 * 
 * 
 */

#include <mcp4261.h>
#include <SPI.h>
#include <FastLED.h>

//const int SPI_CLOCK = 13; //SPI pins ////FastLED.h library defines this pin already////
const int SPI_MOSI = 11;
const int SPI_MISO = 12;

 //Chip select pin for mcp4261 
const int POT_0_CS = 10;
const int POT_1_CS = 9;

MCP4261 pot0 = MCP4261(POT_0_CS);   //ditital pot chip select 
MCP4261 pot1 = MCP4261(POT_1_CS);

int wiper_0_pot_0 = 200; //starts digipot wiper position at 200
int wiper_1_pot_0 = 200;
int wiper_0_pot_1 = 200;
int wiper_1_pot_1 = 200;

const int freq1_pin = 5; //2175 pinout
const int freq2_pin = 3; //1004 pinout

const int period1 = 230; //period for 2175hz (in microseconds)
const int period2 = 498; //perios for 1004hz (in microseconds)
const int period3 = 244; //perios for 2050hz (in microseconds)

int EIA_period = period1; //for EIA tone freq shifting

const int HLGT_level = 200;  //sets digital pot for EIA tone level
const int LLGT_level = 100;  //sets digital pot for EIA tone level
const int FUNC_level = 190;  //sets digital pot for EIA tone level
int prev_pot_lvl = 200;    //to store pot value before triggering EIA

unsigned long currentMicros = 0;       //tracks microseconds board is on
unsigned long currentMillis = 0;       //tracks milliseconds board is on
unsigned long previous_freq1_micros = 0;        //for timing frequency 
unsigned long previous_freq2_micros = 0;
unsigned long EIA_HLGT_Millis = 0;     //for Timing EIA duration
unsigned long EIA_FUNC_Millis = 0;
unsigned long EIA_LLGT_Millis = 0;
unsigned long previous_EIA_state_machine_Millis = 0; //for timing eia state machine functions 
unsigned long previous_EIA_state_timer_Millis = 0;
unsigned long loopdelay_Millis = 0;
const int Loop_interval = 20;
const int EIA_state_machine_interval = 1;

byte freq1_state = LOW; //2175       //state of freq output (creates squarewave)
byte freq2_state = LOW; //1004
byte call1004freq = LOW; //turns 1004Hz on/off
byte call2175freq = LOW; //turns 2175Hz on/off
byte call2050freq = LOW; //turns 2050Hz on/off
byte HLGT_state = LOW; //turns HLGT on/off
byte FUNC_state = LOW;
byte LLGT_state = LOW;
byte EIA_trig = LOW;

//float rAB_ohms = 5090.00; // 5k Ohm      //for MCP4261 official library
//MCP4261 Mcp4261 = MCP4261( MCP4261_SLAVE_SELECT_PIN, rAB_ohms ); //for MCP4261 official library

char key_in;
/*
 * LED SECTION 
 */
 #include "FastLED.h"

// How many leds are in the strip?
#define NUM_LEDS 2

// Data pin that led data will be written out over
#define DATA_PIN 6

// Clock pin only needed for SPI based chipsets when not using hardware SPI
//#define CLOCK_PIN 8

// This is an array of leds.  One item for each led in your strip.
CRGB leds[NUM_LEDS];

//Button Pins
const int Button1 = 7;
const int Button2 = 8;

//Button States (to control things)
byte Button1_state = LOW;
byte Button2_state = LOW;

//Button timer / Button Interval
unsigned long Button1_Millis = 0;  //button 1 timer
unsigned long Button2_Millis = 0;  //button 2 timer
unsigned long previous_Button1_do_Millis = 0; //button1 write timer
unsigned long previous_Button2_do_Millis = 0; //button2 write timer
const int Button_interval = 250;  //used for both buttons

/*******************************************************************************
 *************************SETUP*************************************************
 *******************************************************************************
 */
void setup() {
  // Put code Here
    // Setup SPI communications
    SPI.setDataMode(SPI_MODE0);
    SPI.setBitOrder(MSBFIRST);
    SPI.setClockDivider(SPI_CLOCK_DIV8);
   SPI.begin();

      pot0.initialize(); //initializes pot0 & 1
      pot1.initialize();
 Serial.begin(9600);
 Serial.println("EIA Tone Generator booted");
    //sets pins
 pinMode(freq1_pin, OUTPUT);
 pinMode(freq2_pin, OUTPUT);
 pinMode(POT_0_CS , OUTPUT);
 pinMode(POT_1_CS , OUTPUT);
 pinMode(Button1 , INPUT_PULLUP);
 pinMode(Button2 , INPUT_PULLUP);
 
 FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);

      leds[0] = CRGB::Green;   //toggles LED's at startup
      FastLED.show();
        delay(200);
      leds[1] = CRGB::Blue;
      FastLED.show();
             delay(800);
      leds[0] = CRGB::Black;
      FastLED.show();
        delay(200);
      leds[1] = CRGB::Black;
      FastLED.show();
             delay(10); 
}      
/******************************************************************************
 ******************************LOOP********************************************
 ******************************************************************************
 */
void loop() {
  // track timing
currentMicros = micros();
currentMillis = millis();
//Terminal input
readinput(); //reads serial input from terminal and increments / deincrements digital pots

//if (currentMillis - loopdelay_Millis >= Loop_interval){
//      readButton1();  //pin7
//      readButton2();  //pin8
//      Button1_do(); //button 1 action
//      Button2_do(); //button 2 action
    //  EIA_state_timer(); //starts EIA timer if trig goes high
 //     EIA_state_machine(); //Runs EIA state machine code
//      writeEIA_state ();
//      loopdelay_Millis += Loop_interval;
 /*Serial.println(freq1_state);
 Serial.println("freq 1 State");
 Serial.println(freq2_state);
 Serial.println("freq 2 State");
 Serial.println(call1004freq);
 Serial.println("call1004freq");
 Serial.println(call2175freq);
 Serial.println("call2175freq");
  Serial.println(call2050freq);
 Serial.println("call2050freq");
  Serial.println(HLGT_state);
 Serial.println("HLGT_state");
  Serial.println(FUNC_state);
 Serial.println("FUNC_state");
  Serial.println(LLGT_state);
 Serial.println("LLGT_state"); 
  Serial.println(EIA_trig);
 Serial.println("EIA_trig"); */
 
// }

freq1_controller(); // generates square wave on freq1 and/or freq2 pin at freq determined by period1, period2 or period3
freq2_controller();



}
/******************************************************************************
 *************************END OF LOOP****************************************** 
 ****************************************************************************** 
 */

void readButton1() {

     // this only reads the button state after the button interval has elapsed
     //  this avoids multiple flashes if the button bounces
     // every time the button is pressed it changes button1_State causing the button to toggle on or off
     // Notice that there is no need to synchronize this use of millis()
 
 if (millis() - Button1_Millis >= Button_interval) {

   if (digitalRead(Button1) == LOW) {
     Button1_state = ! Button1_state; // this changes it to LOW if it was HIGH 
                                          //   and to HIGH if it was LOW
     Button1_Millis += Button_interval;
   }
 }

}
void readButton2() {

     // this only reads the button state after the button interval has elapsed
     //  this avoids multiple flashes if the button bounces
     // every time the button is pressed it changes button2_State causing the button to toggle on or off
     // Notice that there is no need to synchronize this use of millis()
 
 if (millis() - Button2_Millis >= Button_interval) {

   if (digitalRead(Button2) == LOW) {
     Button2_state = ! Button2_state; // this changes it to LOW if it was HIGH 
                                          //   and to HIGH if it was LOW
     Button2_Millis += Button_interval;
   }
 }

}
/***********************************************Button 1 Action***********************************************/
void Button1_do() {  
     if (currentMillis - previous_Button1_do_Millis >= Button_interval){ 
        
        if (Button1_state == HIGH) {
           // EIA_trig = HIGH;
         call1004freq = HIGH; //turns 1004Hz on/off
         call2175freq = LOW;
          leds[1] = CRGB::Blue;  
          FastLED.show();
      } else {
          call1004freq = LOW;
          leds[1] = CRGB::Black;  
          FastLED.show();
      }
    previous_Button1_do_Millis += Button_interval;
    }
}
/***********************************************Button 2 Action***********************************************/
void Button2_do() {  
     if (currentMillis - previous_Button2_do_Millis >= Button_interval){ 
        
        if (Button2_state == HIGH) {
           // EIA_trig = HIGH;
         call1004freq = LOW; //turns 1004Hz on/off
         call2175freq = HIGH;
          leds[0] = CRGB::Red;  
          FastLED.show();
      } else {
          call2175freq = LOW;
          leds[0] = CRGB::Black;  
          FastLED.show();
      }
    previous_Button2_do_Millis += Button_interval;
    }
}
/********************************************************************************
 **********Increments/Deincrements Pot Wipers- Commands from serial in*********** 
 ******************************************************************************** 
 */
void wiper_0_pot_0_inc(){            // increments pot of final output amp

      wiper_0_pot_0++;
      Serial.println(wiper_0_pot_0);
  if (wiper_0_pot_0 >= 256){
    wiper_0_pot_0 = 256;
  }
    if (wiper_0_pot_0 <= 0){
    wiper_0_pot_0 = 0;
  }
  digipot0_write();
}

void wiper_0_pot_0_dec(){           // deincrements pot of final output amp

      wiper_0_pot_0--;
      Serial.println(wiper_0_pot_0);  
    if (wiper_0_pot_0 >= 256){
    wiper_0_pot_0 = 256;
  }
    if (wiper_0_pot_0 <= 0){
    wiper_0_pot_0 = 0;
  }
  digipot0_write();
}

void wiper_1_pot_0_inc(){           // increments input level adjustment pot
       
        wiper_1_pot_0++;
        Serial.println(wiper_1_pot_0);  
  if (wiper_1_pot_0 >= 256){
    wiper_1_pot_0 = 256;
  }
    if (wiper_1_pot_0 <= 0){
    wiper_1_pot_0 = 0;
  }
  digipot0_write();
}

void wiper_1_pot_0_dec() {            // deincrements input level adjustment pot

      wiper_1_pot_0--;
      Serial.println(wiper_1_pot_0);  
    if (wiper_1_pot_0 >= 256){
    wiper_1_pot_0 = 256;
  }
    if (wiper_1_pot_0 <= 0){
    wiper_1_pot_0 = 0;
  } 
  digipot0_write(); 
}

void wiper_0_pot_1_inc(){             // increments pot for freq1_pin level adjustment (2175 hz/2050 hz)
        
        wiper_0_pot_1++;
        Serial.println(wiper_0_pot_1);  
  if (wiper_0_pot_1 >= 256){
    wiper_0_pot_1 = 256;
  }
    if (wiper_0_pot_1 <= 0){
    wiper_0_pot_1 = 0;
  }
  digipot1_write();
}

void wiper_0_pot_1_dec(){             // deincrements pot for freq1_pin level adjustment (2175 hz/2050 hz)

      wiper_0_pot_1--;
      Serial.println(wiper_0_pot_1);  
    if (wiper_0_pot_1 >= 256){
    wiper_0_pot_1 = 256;
  }
    if (wiper_0_pot_1 <= 0){
    wiper_0_pot_1 = 0;
  }
  digipot1_write();  
}

void wiper_1_pot_1_inc(){             // increments pot for freq2_pin level adjustment (1004 hz)
      
      wiper_1_pot_1++;
      Serial.println(wiper_1_pot_1);  
    if (wiper_1_pot_1 >= 256){
    wiper_1_pot_1 = 256;
  }
    if (wiper_1_pot_1 <= 0){
    wiper_1_pot_1 = 0;
  }
  digipot1_write();  
}
 
void wiper_1_pot_1_dec() {         // deincrements pot for freq2_pin level adjustment (1004 hz)

      wiper_1_pot_1--;
      Serial.println(wiper_1_pot_1);  
    if (wiper_1_pot_1 >= 256){
    wiper_1_pot_1 = 256;
  }
    if (wiper_1_pot_1 <= 0){
    wiper_1_pot_1 = 0;
  }
  digipot1_write();  
}
/*********************************************************************************
 **************Reads Serial input from terminal*********************************** 
 ********************************************************************************* 
 */
                   //reads serial input from terminal
                   //then adjust digital pot up or down 
void readinput() {                  
  if (Serial.available() > 0) {
    key_in = Serial.read();
    key_in = toupper(key_in);
      Serial.print(key_in);
    switch (key_in) {
      case 'A':
        wiper_0_pot_0_inc();
        break;
      case 'Z':
        wiper_0_pot_0_dec();
        break;
      case 'S':
        wiper_1_pot_0_inc();
        break;
      case 'X':
        wiper_1_pot_0_dec();
        break;
      case 'D':
        wiper_0_pot_1_inc();
        break;        
      case 'C':
        wiper_0_pot_1_dec();
        break;
      case 'F':
        wiper_1_pot_1_inc();
        break;
      case 'V':
        wiper_1_pot_1_dec();
        break;        
      case 'E':
        call2175freq = ! call2175freq;
        break;
      case 'T':
        call1004freq = ! call1004freq;
        break;          
    }
  }

}
/**********************************************************************************
 ********************************************************************************** 
 */
//void set_digipot() {
  
 
//}
void EIA_Gen() {        //TBD

    
}
void digipot0_write() {            //writes new value to pot0

     pot0.setWiper0(wiper_0_pot_0);
     pot0.setWiper1(wiper_1_pot_0);  
  }
void digipot1_write() {           //writes new value to pot1

     pot1.setWiper0(wiper_0_pot_1);
     pot1.setWiper1(wiper_1_pot_1);    
  }
  
void freq2_controller() {         //function called in loop, turns 1004hz on/off
  if (call1004freq == HIGH){//if call1004freq is high
    updatefreq2_state();    //updates 1004 state
    write_state();          //writes 1004 state 
  }
  else {
    freq2_state = LOW;      //else output low (no frequency)
  }
}

void freq1_controller() {         //function called in loop, turns 2175hz on/off
  if (call2175freq == HIGH){
    updatefreq1_state();
    write_state();
  }
  else {
    freq1_state = LOW;
  }  
}

void EIA_state_timer() {                   //time the duration of each section of EIA tone, when tone is initiated
//if (currentMillis - previous_EIA_state_timer_Millis >= EIA_state_machine_interval) {
if (EIA_trig == HIGH) {                    //happens once per EIA when EIA_trig is set HIGH
  EIA_HLGT_Millis = currentMillis + 125;
  EIA_FUNC_Millis = EIA_HLGT_Millis + 40;
  EIA_LLGT_Millis = EIA_FUNC_Millis + 100;
  prev_pot_lvl = wiper_0_pot_1;            //saves prev pot value
  wiper_0_pot_1 = HLGT_level;              //sets High Level guard tone Level
  EIA_period = period1;                    //set period for 2175HZ
  pot1.setWiper0(wiper_0_pot_1);           //writes to pot
  EIA_trig = LOW;                          //resets EIA trigger
  }
 //previous_EIA_state_timer_Millis += EIA_state_machine_interval;
// }
}
  
void EIA_state_machine() {                           //turn tone on
//if (currentMillis - previous_EIA_state_machine_Millis >= EIA_state_machine_interval){  
  if (currentMillis - EIA_HLGT_Millis <= 125) { //High level guard tone
      HLGT_state = HIGH;
  } else { HLGT_state = LOW; }
  if (EIA_FUNC_Millis - currentMillis >= 1 && EIA_HLGT_Millis - currentMillis < 1){
      FUNC_state = HIGH; 
      EIA_period = period3;
      wiper_0_pot_1 = FUNC_level;        // saves Function level to pot
  } else { FUNC_state = LOW; }
  if (EIA_LLGT_Millis - currentMillis >= 1 && EIA_FUNC_Millis - currentMillis < 1){
      LLGT_state = HIGH;
      EIA_period = period1;
      wiper_0_pot_1 = LLGT_level;       //saves Low level guard tone "level" to pot
  } else { LLGT_state = LOW; }
        if (EIA_LLGT_Millis - currentMillis < 10 && EIA_LLGT_Millis - currentMillis > 1) {
        wiper_0_pot_1 = prev_pot_lvl;
        call2175freq = LOW;
        }
 // previous_EIA_state_machine_Millis += EIA_state_machine_interval;
 // }
}
void writeEIA_state (){
    if (HLGT_state == HIGH || LLGT_state == HIGH || FUNC_state == HIGH) {             
      call2175freq = HIGH;
   } 
}
void updatefreq1_state() { //2175Hz/2050HZ //this part makes the state of the output change from low to hi to low etc. based on the period
  if (freq1_state == LOW) {
    if (currentMicros - previous_freq1_micros >= EIA_period) {
      freq1_state = HIGH;
      previous_freq1_micros += EIA_period;
    }
  } 
  else {
    if (currentMicros - previous_freq1_micros >= EIA_period) {
      freq1_state = LOW;
      previous_freq1_micros += EIA_period;
    }
    
  }
}

void updatefreq2_state() { //1004Hz
  if (freq2_state == LOW) {
    if (currentMicros - previous_freq2_micros >= period2) {
      freq2_state = HIGH;
      previous_freq2_micros += period2;
    }
  } else {
    if (currentMicros - previous_freq2_micros >= period2) {
      freq2_state = LOW;
      previous_freq2_micros += period2;
    }
    
  }  
}

void write_state() {
 digitalWrite(freq1_pin, freq1_state);
 digitalWrite(freq2_pin, freq2_state);  
}
