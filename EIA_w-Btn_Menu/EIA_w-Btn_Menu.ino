/*2 button 2 led menu
 * RED Button              GREEN Button
 * "Hold" for menu         Adv. in menu
 * 
 * 
 * 
 * 
 * 
 * 
 */
#include <mcp4261.h>
#include <SPI.h>
#include <FastLED.h>


//const int SPI_CLOCK = 13; //SPI pins ////FastLED.h library defines this pin already////
const int SPI_MOSI = 11;
const int SPI_MISO = 12;
 //Chip select pin for mcp4261 
const int POT_0_CS = 10;
const int POT_1_CS = 9;
MCP4261 pot0 = MCP4261(POT_0_CS);   //ditital pot chip select 
MCP4261 pot1 = MCP4261(POT_1_CS);
int wiper_0_pot_0 = 200; //starts digipot wiper position at 200
int wiper_1_pot_0 = 200;
int wiper_0_pot_1 = 200;
int wiper_1_pot_1 = 200;

const int freq1_pin = 5; //2175 pinout
const int freq2_pin = 3; //1004 pinout

#define buttonPin 7 // analog input pin to use as a digital input
#define button2Pin 8
const int lvlLED = 0; // digital output pin for LED 1 indicator
const int funLED = 1; // digital output pin for LED 2 indicator

#define debounce 100 // ms debounce period to prevent flickering when pressing or releasing the button
#define holdTime 1000 // ms hold period: how long to wait for press+hold event

// Button variables
int buttonVal = 0; // value read from button 1
int button2Val = 0; // value read from button 2
int buttonLast = 0; // buffered value of the button 1's previous state
int button2Last = 0; // buffered value of the button 2's previous state
long btnDnTime; // time the button 1 was pressed down
long btn2DnTime; // time the button 2 was pressed down
long btnUpTime; // time the button was released
long btn2UpTime; // time the button was released
boolean ignoreUp = false; // whether to ignore the button release because the click+hold was triggered
boolean ignoreUp2 = false;

//Button State variables
int RED_push = 0;
int RED_HOLD = 0;
int GRN_push = 0;
int GRN_HOLD = 0;

// LED variables
int ledVal1 = 0; // state of LED 1
int ledVal2 = 0; // state of LED 2


#define NUM_LEDS 2// How many leds are in the strip?
#define DATA_PIN 6// Data pin that led data will be written out over
CRGB leds[NUM_LEDS];// This is an array of leds.  One item for each led in your strip.


//=================================================


void setup() 
{
    // Setup SPI communications
SPI.setDataMode(SPI_MODE0);
SPI.setBitOrder(MSBFIRST);
SPI.setClockDivider(SPI_CLOCK_DIV8);
SPI.begin();

Serial.begin(9600); 
Serial.println("EIA Tone Generator booted");
    
pot0.initialize(); //initializes pot0 & 1
pot1.initialize();


// Set button input pin
pinMode(buttonPin, INPUT_PULLUP);
//digitalWrite(buttonPin, HIGH );
pinMode(button2Pin, INPUT_PULLUP);
//digitalWrite(button2Pin, HIGH );

FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);

pinMode(freq1_pin, OUTPUT);//Freq pin output modes
pinMode(freq2_pin, OUTPUT);

pinMode(POT_0_CS , OUTPUT); //Pot chip select output mode
pinMode(POT_1_CS , OUTPUT);

/*******************************************************************
 *            Toggle LED's at Startup
 ******************************************************************* 
 */
      leds[0] = CRGB::Green;   //toggles LED's at startup
      FastLED.show();
        delay(200);
      leds[1] = CRGB::Blue;
      FastLED.show();
             delay(800);
      leds[0] = CRGB::Black;
      FastLED.show();
        delay(200);
      leds[1] = CRGB::Black;
      FastLED.show();
             delay(10); 
}

/******************************************************************************
 ******************************LOOP********************************************
 ******************************************************************************
 */
//=================================================


void loop()
{
/********************************************************************/  
// Read the state of the button 1
buttonVal = digitalRead(buttonPin);

      // Test for button pressed and store the down time
      if (buttonVal == LOW && buttonLast == HIGH && (millis() - btnUpTime) > long(debounce))
            {
            btnDnTime = millis();
            }

        // Test for button release and store the up time
        if (buttonVal == HIGH && buttonLast == LOW && (millis() - btnDnTime) > long(debounce))
              {
              if (ignoreUp == false) event1();
              else ignoreUp = false;
              btnUpTime = millis();
              }
  
          // Test for button held down for longer than the hold time
          if (buttonVal == LOW && (millis() - btnDnTime) > long(holdTime))
                {
                event2();
                ignoreUp = true;
                btnDnTime = millis();
                }

  buttonLast = buttonVal;
/********************************************************************/  
// Read the state of the button 2

button2Val = digitalRead(button2Pin);

      // Test for button pressed and store the down time
      if (button2Val == LOW && button2Last == HIGH && (millis() - btn2UpTime) > long(debounce))
            {
            btn2DnTime = millis();
            }

        // Test for button release and store the up time
        if (button2Val == HIGH && button2Last == LOW && (millis() - btn2DnTime) > long(debounce))
              {
              if (ignoreUp2 == false) event3();
              else ignoreUp2 = false;
              btn2UpTime = millis();
              }
  
          // Test for button held down for longer than the hold time
          if (button2Val == LOW && (millis() - btn2DnTime) > long(holdTime))
                {
                event4();
                ignoreUp2 = true;
                btn2DnTime = millis();
                }

  button2Last = button2Val;
/***************************************************************************/


}

//=================================================
// Events to trigger by click and press+hold

void event1() //RED Button Push
{
RED_push++;
if (RED_push > 6){
    RED_push = 0; 
}
Serial.println(RED_push);
delay(10);
}

void event2() //RED Button HOLD
{
RED_HOLD++;
if (RED_HOLD > 6){
    RED_HOLD = 0;
}
    if (RED_HOLD == 1){
      leds[1] = CRGB::Blue;
      FastLED.show();
}
if (RED_HOLD == 2){
      leds[1] = CRGB::Red;
      FastLED.show();
}
if (RED_HOLD == 3){
      leds[1] = CRGB::Green;
      FastLED.show();
}
if (RED_HOLD == 4){
      leds[1] = CRGB::Yellow;
      FastLED.show();
}
if (RED_HOLD == 5){
      leds[1] = CRGB::White;
      FastLED.show();
}
if (RED_HOLD == 6){
      leds[1] = CRGB::Black;
      FastLED.show();
} 
  
Serial.println("Btn 1 HOLD");
delay(10);
}
void event3() //Green Button Push
{
  GRN_push++;
  if (GRN_push > 6){
    GRN_push = 0;
  }
  
Serial.println("Btn 2 Push");
delay(10);
}

void event4() //Green Button HOLD
{
  GRN_HOLD++;
    if (GRN_HOLD > 6){
    GRN_HOLD = 0;
    }
    if (GRN_HOLD == 1){
      leds[0] = CRGB::Blue;
      FastLED.show();
}
if (GRN_HOLD == 2){
      leds[0] = CRGB::Red;
      FastLED.show();
}
if (GRN_HOLD == 3){
      leds[0] = CRGB::Green;
      FastLED.show();
}
if (GRN_HOLD == 4){
      leds[0] = CRGB::Orange;
      FastLED.show();
}
if (GRN_HOLD == 5){
      leds[0] = CRGB::White;
      FastLED.show();
}
if (GRN_HOLD == 6){
      leds[0] = CRGB::Black;
      FastLED.show();
}
  
Serial.println("Btn 2 HOLD");
delay(10);
}
